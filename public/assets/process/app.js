var app = angular.module('app', ['ui.router']);

app.config(function($stateProvider, $urlRouterProvider, $locationProvider,  $httpProvider){

	$stateProvider
    .state('home', {
      url: '/',
      views: {
        'header': {
          templateUrl: 'templates/menus/header.html'
        },
        'main': {
          templateUrl: 'templates/home.html'
        },
        'side-menu': {
          templateUrl: 'templates/side-menu.html'
        }
      },
    })
    

    $urlRouterProvider.otherwise('/');
});